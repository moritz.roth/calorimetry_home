#Calorimetry-Küschentischversuch
#Hauptfunktionsskript indem alle anderen Funktionen miteinader vereint werden, um mit den Sensoren Temperaturen zu messen und diese in Hdf5 Datein zu speichen

from functions import m_json
from functions import m_pck

# creating paths to folders and fils we need for the newton experiment
data_folder_path = "data/data_newton"
setup_path = "datasheets/setup_newton.json"

#creating paths to folders and fils we need for the heat capacity experiment
data_folder_path = "data/data_heat_capacity"
setup_path = "datasheets/setup_heat_capacity.json"

#creating metadata dict from the json file
metadata = m_json.get_metadata_from_setup(setup_path)

#adding serial ids to metadata
m_json.add_temperature_sensor_serials("datasheets", metadata)

#measuring temperature and putting into dict with temperature and time
data = m_pck.get_meas_data_calorimetry(metadata)

#creating datafolder, saving data in a h5 file and associated meta data in json file
m_pck.logging_calorimetry(data,metadata,data_folder_path,"datasheets")

#creating a json file from current experiment in the data folder
m_json.archiv_json("datasheets",setup_path,data_folder_path)

